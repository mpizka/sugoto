// demo for a solver using sugoto

package main

import (
    "bufio"
    "fmt"
    "os"
    puzzle "gitlab.com/mpizka/sugoto/sugotoPuzzleExample"
    "gitlab.com/mpizka/sugoto"

)

func main () {
    // dont do that...
    defer func() {
        p := recover()
        fmt.Println(p)
        os.Exit(2)
    }()

    fmt.Println(`
Standard 9x9 sudoku solver
Input field sequence, confirm with ENTER
Partial sequences are allowed, solving once field is full
Empty fields have value 0
`)

    stdin := bufio.NewScanner(os.Stdin)

    // get puzzle from command line
    pzl := make([]int,0)
    fmt.Printf("> ")
    for stdin.Scan() {
        txt := stdin.Text()
        for _,letter := range txt {
            letter -= '0'
            if letter < 0 || letter > 9 {
                panic ("(!) Cannot process non-numerical input!")
            }
            pzl = append(pzl, int(letter))
        }

        l := len(pzl)
        switch {
        case l < 9*9 :
            fmt.Println("(i)", 9*9-len(pzl), "remaining")
        case l > 9*9 :
            fmt.Println("(!) Puzzle is too long, truncating...")
            pzl = pzl[0:9*9]
            fallthrough
        case l == 9*9 :
            ex := puzzle.New(pzl)
            fmt.Println(ex.String())
            if sugoto.Solve(ex) {
                fmt.Println("(*) Puzzle solved!")
                fmt.Println(ex.String())
            } else {
                fmt.Println("(-) Puzzle is not solveable")
            }
            pzl = make([]int,0)
        }
        fmt.Printf("> ")
    }
}
