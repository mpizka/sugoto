// Package sugoto is an extendable solver for sudoku-like puzzles written as an exercise in learning golang
// It defines an interface Puzzle and a Solve() function. New Puzzles can be defined by implementing a type
// which satisfy Puzzle, and solved by calling Solve() on it.

package sugoto

// Puzzle must be satisfied by any puzzle intended to be solved with Solve()
// A sudoku-like puzzle has fields (the squares to fill in) which must be indentified
// by an integer-index. Fields have positive values of type int which start at 1
type Puzzle interface {
	// Next(field_index) (index_next_unfilled_field, puzzle_is_solved)
	Next(int) (int, bool)
	// TakenAt (field_index) NOT_available_values_at_this_field
	TakenAt(int) map[int]struct{}
	// Set(field_index, new_value_for_field)
	Set(int, int)
	// Reset(field_index) // reset field to its zero (non-filled) value
	Reset(int)
	// UpperBoundary() max numeric value for a puzzle-field
	UpperBoundary() int
}

// Solve a puzzle satisfying the Puzzle-interface using backtracking
func Solve(p Puzzle) bool {
	return backtrack(p, 0)
}

// backtracking function
func backtrack(p Puzzle, n int) bool {
	// get current field, check for solved
	n, solved := p.Next(n)
	if solved {
		return true
	}
	// get taken numbers for curent field
	nums_taken := p.TakenAt(n)
	// get upper boundary
	ub := p.UpperBoundary()

	// try available numbers for n
	for i := 1; i <= ub; i++ {
		_, taken := nums_taken[i]
		if taken {
			continue
		}
		p.Set(n, i)
		if backtrack(p, n+1) {
			return true
		}
	}
	// reset field and backtrack
	p.Reset(n)
	return false
}
