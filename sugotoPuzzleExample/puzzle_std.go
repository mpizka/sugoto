// Package sugotoPuzzleExample implements a type satisfying the sugoto.Puzzle interface
// including a few convenience methods like String and New
// The puzzle type is equivalent to the standard 9x9 Sudoku Puzzles
package sugotoPuzzleExample

import (
	"bytes"
	"fmt"
)

type FieldStandard struct {
	field         [9 * 9]int
	upperBoundary int
}

// New takes a slice of integers and returns a struct representing the puzzle
func New(inp []int) *FieldStandard {
	f := new(FieldStandard)
	copy(f.field[:], inp)
	f.upperBoundary = 9
	return f
}

// String returns a text representation of the Puzzle in its current configuration
func (f *FieldStandard) String() string {
	var output bytes.Buffer

	for i := 0; i < 9*9; i++ {
		if i > 0 && i%9 == 0 {
			fmt.Fprintf(&output, "\n")
		}
		if f.field[i] == 0 {
			output.WriteString("  ")
		} else {
			fmt.Fprintf(&output, "%d ", f.field[i])
		}
	}
	return output.String()
}

/***********************************************/
/*** contract functions for Puzzle-interface ***/
/***********************************************/

// Next returns the next unfilled index of the current grid configuration
// and a boolean indicating if the field has been filled completely, indicating
// a solution to the Puzzle
func (f *FieldStandard) Next(n int) (int, bool) {
	for ; n < len(f.field); n++ {
		if f.field[n] == 0 {
			return n, false
		}
	}
	return -1, true
}

// Set sets the field index i to n
func (f *FieldStandard) Set(i, n int) {
	f.field[i] = n
}

// UpperBoundary reports the max value for a field
func (f *FieldStandard) UpperBoundary() int {
	return f.upperBoundary
}

// Reset resets a field to its "empty-value"
func (f *FieldStandard) Reset(i int) {
	f.field[i] = 0
}

// TakenAt returns a map detailing which numbers are not available
// in the grid at an index, given the current grid-configuration
func (f *FieldStandard) TakenAt(i int) map[int]struct{} {
	m := taken_numbers(
		f.get_sector(i),
		f.get_column(i),
		f.get_line(i),
	)
	return m
}

/*** helpers to TakenAt ***/

func taken_numbers(slcs ...[]int) map[int]struct{} {
	m := make(map[int]struct{})
	for _, sl := range slcs {
		for _, num := range sl {
			if num == 0 {
				continue
			}
			m[num] = struct{}{}
		}
	}
	return m
}

func (f *FieldStandard) get_column(i int) []int {
	sl := make([]int, 0, 10)
	c := i % 9
	for l := 0; l < 9; l++ {
		sl = append(sl, f.field[c+9*l])
	}
	return sl
}

func (f *FieldStandard) get_line(i int) []int {
	l := i / 9
	sl := make([]int, 0, 10)
	sl = append(sl, f.field[l*9:8+l*9]...)
	return sl
}

func (f *FieldStandard) get_sector(i int) []int {
	sl := make([]int, 0, 10)
	// C/L : top left field of sector which has c/l
	C := (i % 9) / 3 // i%9 == c
	L := (i / 9) / 3 // i/9 == l
	C *= 3
	L *= 3
	// TODO: This nested loop is ugly...
	for c := 0; c < 3; c++ {
		for l := 0; l < 3; l++ {
			sl = append(sl, f.field[C+c+9*(L+l)])
		}
	}
	return sl
}
